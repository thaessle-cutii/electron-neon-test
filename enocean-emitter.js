const { EventEmitter } = require('events');
const { promisify } = require('util');
const { EventEmitter: RustChannel } = require('cutii-enocean-node');

let enoceanEmitter;
class EnoceanCommunicator extends EventEmitter {
  constructor(sp) {
    super();
    this._shutdown = false;
    this.channel = new RustChannel(sp); // Channel between Rust and Node part
    const poll = promisify(this.channel.poll.bind(this.channel)); // Polling "hack"
    // Will be replaced when this PR will be merged : https://github.com/neon-bindings/neon/pull/375

    const loop = () => {
      if (this._shutdown) return;
      poll()
        .then(({ event, ...data }) => this.emit(event, data))
        .catch(err => this.emit('channelError', err))
        .then(() => setImmediate(loop));
    };
    loop();
  }

  shutdown() {
    this._shutdown = true;
    return this;
  }

  send_enocean_cmd(eep, id, command) {
    this.channel.send_cmd(eep, id, command);
  }
}
const getEnoceanEmitter = serialport => {
  if (!enoceanEmitter) {
    enoceanEmitter = new EnoceanCommunicator(serialport);
  }
  return enoceanEmitter;
};
