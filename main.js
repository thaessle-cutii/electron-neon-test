const { app, BrowserWindow, Menu, ipcMain } = require('electron');
const getEnoceanEmitter = require('./enocean-emitter');

require('electron-debug')();
app.on('ready', () => {
  const mainWindow = new BrowserWindow({
    show: false,
    width: 1280,
    height: 720,
    frame: true,
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true,
    },
  });

  mainWindow.loadURL('https://www.google.com');
  mainWindow.webContents.on('did-finish-load', () => {
    mainWindow.show();
    mainWindow.focus();
  });
});

//Whatever you realy provide an enocean device the addons is loaded ;)
if (process.env.TTY_ENOCEAN) {
  const serialport = `/dev/${process.env.TTY_ENOCEAN}`;
  const myEmitter = getEnoceanEmitter(serialport);
  // DO SOMETHING WITH myEmitter :)
}
